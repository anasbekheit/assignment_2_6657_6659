package parentClasses;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "dish")
@XmlAccessorType (XmlAccessType.FIELD)
public  class Dish {
	public Dish() {
		
		super();
		}
	@XmlElement(name="count")
	private int count;
	@XmlElement(name = "price")
		private Double price;
	@XmlElement(name = "tax")
		private Double tax;
	@XmlElement(name = "name")
		private String name;
	
	@XmlElement(name = "type")
		private String type;
	
	
		public Dish(String name, Double price , String type) {
			this.setName(name);
			this.setPrice(price);
			this.setType(type);
		}
		public double getTax() {
			if(this.getType().equals("Appetizer"))
				this.tax=0.1;
			else if(this.getType().equals("Desert"))
				this.tax=0.2;
			else
				this.tax=0.15;
			return tax;
		}
		public void setTax(Double d) {
			this.tax = d;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(Double price) {
			this.price = price;
		}
		public double getTotalPrice()
		{
			return this.getPrice()+this.getPrice()*this.getTax();
		}
		@Override
		public String toString() {
			return String.format("%s %.2f$\n", this.getName(),this.getTotalPrice());
		}
		public int getCount() {
			return count;
		}
		public void setCount(int count) {
			this.count = count;
		}
}
