package parentClasses;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import resandorder.Order;

@XmlRootElement(name = "table")
@XmlAccessorType (XmlAccessType.FIELD)
public class Table {
	
	
	public Table() {
	super();
	}

	@XmlElement(name="booked")
	private boolean booked;
	@XmlElement(name="order")
	private Order order= new Order();
	@XmlElement(name="number")
	private  Integer number;
	
	@XmlElement(name="number_of_seats")
	private Integer number_of_seats;
	
	@XmlElement(name="smoking")
	private Boolean smoking;
	
	@XmlElement(name="customerName")
	private String customerName= new String();
	
	
	public Table(int number,int number_of_seats,boolean smoking) {
		super();
		this.setNumber(number);
		this.setNumber_of_seats(number_of_seats);
		this.setSmoking(smoking);
		this.setCustomerName(null);
		this.setBooked(false);
	}
	public Table(int number,int number_of_seats,boolean smoking,boolean booked) {
		super();
		this.setNumber(number);
		this.setNumber_of_seats(number_of_seats);
		this.setSmoking(smoking);
		this.setCustomerName(null);
		this.setBooked(booked);
	}

	public boolean isSmoking() {
		return smoking;
	}

	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}



	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}


	public int getNumber_of_seats() {
		return number_of_seats;
	}


	public void setNumber_of_seats(int number_of_seats) {
		this.number_of_seats = number_of_seats;
	}


	public boolean isBooked() {
		return booked;
	}


	public void setBooked(boolean booked) {
		this.booked = booked;
	}
	public Order getOrder() {
		System.out.println("table class");
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
public String toString() {
	return String.format("Table(%s)\tCustomer Name: %s\n",Integer.toString(this.getNumber()),this.getCustomerName());
}
	
}
