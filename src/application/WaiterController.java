package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import dataHandle.Data;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mainRestaurant.Restaurant;
import parentClasses.Table;
import userTypes.User;
import userTypes.Waiter;

public class WaiterController implements Initializable{
private Waiter waiter;
	
	private Restaurant restaurant;
	
	@FXML
	private Label reservationsLabel;
	@FXML
	private Label ordersLabel;
	@FXML
	private Label comboLabel;
	@FXML
	private TextArea reservationsArea ;
	@FXML
	private TextArea ordersArea ;
	@FXML
	private ComboBox<Table> comboBox;
	@FXML
	private Button serveButton ;
	@FXML
	private Button logoutButton;
	ObservableList<Table> tableList= FXCollections.observableArrayList();
	ObservableList<Table> orderList= FXCollections.observableArrayList();
	//private int userIndex;
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		 comboBox.setItems(orderList);
	}

	public void setWaiter(User user, int userIndex, Restaurant restaurant) {
		this.waiter= new Waiter(user.getName(),user.getRole(),user.getUsername(),user.getPassword());
		this.restaurant=new Restaurant();
		this.restaurant=restaurant;
			restaurant.getUsers().getWaiters().getWaiterList().add(waiter);
		//	userIndex=restaurant.getUsers().getCustomers().getCustomerList().indexOf(cooker);
		 for(Table table :restaurant.getTables().getTables()) {
			 System.out.println(table.getCustomerName());
			 if(table.isBooked()==true) {
				 	 if(table.getOrder().isCooked()==true) {
				 orderList.add(table);	
				 
			 }
					 tableList.add(table);
				 	 System.out.println(table);
				 
			 }
		
			
		 }
		 reservationsArea.setText(tableList.toString());
		 ordersArea.setText(orderList.toString());		
		}
	
	public void serveButtonAction(ActionEvent event) {
		if(comboBox.getValue()!=null) {
			
			waiter.serveOrder(comboBox.getValue(), restaurant);
			tableList.remove(comboBox.getValue());
			orderList.remove(comboBox.getValue());
			reservationsArea.clear();
			ordersArea.clear();
			if(orderList.isEmpty()==false)
			{
				ordersArea.setText(orderList.toString());
			}
			else
			{
				ordersArea.setPromptText("No Orders Are ready Now!");
			
			}
			if(tableList.isEmpty()==false)
			{
				reservationsArea.setText(tableList.toString());
			}
			else
			{
				reservationsArea.setPromptText("No Reservations!");
			
			}
		
		}
		else {
			Alert.display("Error", "Please Select an Order To Serve");
		}
			}
	
	public void logOutButtonAction(ActionEvent event) throws IOException, JAXBException {
		Data data = new Data();
		data.save(restaurant);
		((Node)event.getSource()).getScene().getWindow().hide();
		Stage primaryStage = new Stage();
		primaryStage.initStyle(StageStyle.TRANSPARENT);
		FXMLLoader loader = new FXMLLoader();
		Pane root = loader.load(getClass().getResource("/login.fxml").openStream());
		MainController mainController = (MainController)loader.getController();
		mainController.setRestaurant(restaurant);
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	public void close(ActionEvent event) {
		System.exit(0);
	}
		

}
