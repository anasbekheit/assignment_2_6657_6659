package application;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mainRestaurant.Restaurant;
import parentClasses.Table;
import userTypes.Customer;

 public class CustomerController implements Initializable {

	private int customerIndex;
	private Customer customer = new Customer();
	private Restaurant restaurant;
	@FXML
	private TextField seatText; 
	@FXML
	private ComboBox<String> smokeBox;	
	@FXML
	 private Button reserveButton;
	@FXML
	 private Button closeButton;
	@FXML
	private Label reserveLabel; 
	@FXML
	private Label welcomeLabel; 
	@FXML
	private Label availableLabel;

	ObservableList<String> boolList= FXCollections.observableArrayList("Smoker Table","Non-Smoker Table");
	ObservableList<Integer> seatList= FXCollections.observableArrayList(1,2,3,4,5,6,7,8,9,10);
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		 smokeBox.setItems(boolList);
		 smokeBox.setValue("Non-Smoker Table");
	}
	public void close(ActionEvent event) {
		System.exit(0);
	}

	public void setCustomer(Customer customer,int customerIndex, Restaurant restaurant) {
		this.customer= customer;
		this.customerIndex=customerIndex;
		this.restaurant=restaurant;
		welcomeLabel.setText("Welcome Mr."+customer.getName()+"!");
	}
	public void buttonAction(ActionEvent event) throws IOException {
		if (check()==null)
		{
			if(isInt(seatText)!=-1) {
		int seats=isInt(seatText);
	
		System.out.println("button pressed"+ seatText.getText());
		boolean smoke=smokeBox.getValue()=="Smoker Table"? true:false;
		System.out.println(Boolean.toString(smoke));
		Table table =customer.reserveTable(seats, smoke,this.restaurant);
		if(table== null)
		{
			availableLabel.setTextFill(Color.RED);
			availableLabel.setText("No Tables available please Comeback later!");
			System.out.println("Comeback Later");
		}
		
		else {
			System.out.println("RESERVED SUCCEFULLY Mr with index: "+customer.getName()+customerIndex);
			restaurant.getUsers().getCustomers().getCustomerList().set(customerIndex, customer);
			((Node)event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/OrderWindow.fxml").openStream());
			OrderWindowController orderWindowController = (OrderWindowController)loader.getController();
			orderWindowController.setCustomer(customer,restaurant.getUsers().getCustomers().getCustomerList().indexOf(customer),restaurant);
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		}
			
		}
		else {
			Alert.display("Error", "Please enter a valid seat number (0 -> ...)");
		}
		}
		else {
			Alert.display("Error", "Mr."+customer.getName()+" you already have a reservation at table number: "+check().getNumber());
			((Node)event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			primaryStage.initStyle(StageStyle.TRANSPARENT);
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/Login.fxml").openStream());
			MainController mainController = (MainController)loader.getController();
			mainController.setRestaurant(restaurant);
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		}
		}
	
	private Table check() {
		for(Table table : restaurant.getTables().getTables()) {
			if(table.isBooked()==true&& table.getCustomerName().equals(customer.getName())) {
				return table;
			}
		}
		return null;
	}
	private int isInt(TextField input)
	{
		try {
			int seat= Integer.parseInt(input.getText());
			if(seat>0)
			{
					return seat;
			}
			else return -1;
		}catch (NumberFormatException e) {
			
		}
		System.out.println("ERROR");
		return -1;
	}
	public Restaurant getRestaurant() {
		return restaurant;
	}
	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	}
 

