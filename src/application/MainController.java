package application;


import java.net.URL;
import java.util.ResourceBundle;



import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mainRestaurant.Restaurant;
import userTypes.Customer;
import userTypes.User;
public class MainController implements Initializable  {

	@FXML private Label pleaseLabel;
	@FXML private Label userLoginLabel;
	@FXML private Label accountLabel;
	@FXML private Label notMemberLabel;
	@FXML private Label bigLabel;
	@FXML private Hyperlink signUpLink;
	
	@FXML private Button exitButton;
	@FXML private Button loginButton;
	
	@FXML private ImageView patrickStar;
	@FXML private ImageView spongeBob;
	@FXML private ImageView mrKrabs;
	@FXML private ImageView squidWard;
	
	
	@FXML FontAwesomeIconView userIcon = new FontAwesomeIconView(FontAwesomeIcon.USER);
	@FXML FontAwesomeIconView lockIcon = new FontAwesomeIconView(FontAwesomeIcon.LOCK);
	@FXML FontAwesomeIconView unlockIcon = new FontAwesomeIconView(FontAwesomeIcon.UNLOCK);
	@FXML FontAwesomeIconView usersIcon = new FontAwesomeIconView(FontAwesomeIcon.USERS);
	@FXML FontAwesomeIconView exitIcon = new FontAwesomeIconView(FontAwesomeIcon.CLOSE);
	
	@FXML
	private TextField txt_userName;
	
	@FXML
	private PasswordField txt_passWord;
	private Restaurant restaurant;
	private Customer customer;
	private int userIndex;

	@FXML
	public void exit (ActionEvent event) {
		System.exit(0);
	}

@SuppressWarnings("static-access")
public void Login(ActionEvent event)  throws Exception  {
	System.out.println("PRESSED");
	String userName=txt_userName.getText();
	String passWord=txt_passWord.getText();
	for(User user: restaurant.getUsers().getUserList()) {
		if(user.getUsername().equals(userName) && user.getPassword().equals(passWord))
		{
			userIndex=restaurant.getUsers().getUserList().indexOf(user);

			if(user.getRole().equals("Client")) {
				customer= new Customer(user.getName(),user.getRole(),user.getUsername(),user.getPassword());
				restaurant.getUsers().getCustomers().getCustomerList().add(customer);
				userIndex=restaurant.getUsers().getCustomers().getCustomerList().indexOf(customer);
					System.out.println("Entered IF");
					((Node)event.getSource()).getScene().getWindow().hide();
					Stage primaryStage = new Stage();
					primaryStage.initStyle(StageStyle.TRANSPARENT);
					FXMLLoader loader = new FXMLLoader();
					Pane root = loader.load(getClass().getResource("/Customer.fxml").openStream());
					CustomerController customerController = (CustomerController)loader.getController();
					customerController.setCustomer(customer,userIndex,restaurant);
					Scene scene = new Scene(root);
					scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
					primaryStage.setScene(scene);
					primaryStage.show();
					break;
			}
			else if(user.getRole().equals("Cooker"))
			{
				
				
					((Node)event.getSource()).getScene().getWindow().hide();
					Stage primaryStage = new Stage();
					FXMLLoader loader = new FXMLLoader();
					Pane root = loader.load(getClass().getResource("/Cooker.fxml").openStream());
					CookerController cookerController = (CookerController)loader.getController();
					cookerController.setCooker(user, userIndex, restaurant);
					Scene scene = new Scene(root);
					scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
					primaryStage.setScene(scene);
					primaryStage.show();

			break;
		}
			else if (user.getRole().equals("Waiter")) {
				((Node)event.getSource()).getScene().getWindow().hide();
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/Waiter.fxml").openStream());
				WaiterController waiterController = (WaiterController)loader.getController();
				waiterController.setWaiter(user, userIndex, restaurant);
				Scene scene = new Scene(root);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();
			}
			else if(user.getRole().equals("Manager"))
			{
				((Node)event.getSource()).getScene().getWindow().hide();
				ManagerWindow managerWindow = new ManagerWindow();
				managerWindow.displayOptions(restaurant);
				break;
			}
			
		}
		else
		{
			
			pleaseLabel.setText("Invalid username and/or password");
			pleaseLabel.setTextFill(Color.RED);
		}
		
	}
	txt_userName.clear();
	txt_passWord.clear();
	
}


public Restaurant getRestaurant() {
	return restaurant;
}


public void setRestaurant(Restaurant restaurant) {
	this.restaurant = restaurant;
}
@Override
public void initialize(URL arg0, ResourceBundle arg1) {
	// TODO Auto-generated method stub
	
}
}