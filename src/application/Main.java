package application;

import java.io.IOException;


import dataHandle.Data;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mainRestaurant.Restaurant;
import parentClasses.Dish;

public class Main extends Application{
	
	private double xOffSet=0;
	private double yOffSet=0;
	public static void main(String[] args)  
	{
		
	
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
	
		
		try {
			Restaurant restaurant = new Restaurant();
			Data data = new Data();
			restaurant=data.load();
			for(Dish dish:restaurant.getDishes().getDishes())
			{
				System.out.println(dish);
			}
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/Login.fxml").openStream());
			Scene scene = new Scene(root);
			primaryStage.initStyle(StageStyle.TRANSPARENT);
	
			
		
			MainController mainController = (MainController)loader.getController();
			mainController.setRestaurant(restaurant);	
			
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	
		
	}
	
	

}
	 

		

