package application;
import mainRestaurant.Restaurant;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javax.xml.bind.JAXBException;

import dataHandle.Data;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import parentClasses.Dish;
import resandorder.Order;
import userTypes.Customer;

public class OrderWindowController implements Initializable{
	@FXML
	private Label headLabel;
	@FXML
	private Label dishesLabel;
	@FXML
	private Label orderPriceLabel;
	@FXML
	private ComboBox<Dish> dishBox;
	@FXML
	 private Button addToOrderButton;
	@FXML
	 private Button confirmOrderButton;
	@FXML
	 private Label dollarLabel;
	
	private int customerIndex;
	private Customer customer;
	
	private Order order;
		
	
	ObservableList<Dish> dishList= FXCollections.observableArrayList();
	private Restaurant restaurant;
	
	

	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
			
		
			 this.order= new Order();
			 this.dollarLabel.setText("0.00$");
			 
			
			        
		 }
		
	

	public void setCustomer(Customer customer, int customerIndex, Restaurant restaurant) {
		// TODO Auto-generated method stub
		this.customer=customer;
		this.customerIndex=customerIndex;
		this.restaurant=new Restaurant();
		this.restaurant=restaurant;
		 for(Dish dish :this.restaurant.getDishes().getDishes()) {
			 dishBox.getItems().add(dish);
			 
		 }
	}
	
	
	public void addToOrder(ActionEvent event) throws IOException {
		if(dishBox.getValue()!=null) {
				this.order.addToOrder(dishBox.getValue());
		this.dollarLabel.setTextFill(Color.LIMEGREEN);
		this.dollarLabel.setText(Double.toString(this.order.getTotalPrice())+"$");
		System.out.println("PRESSED");
		}
		else {
			Alert.display("Error", "Order must contain atleast one dish");
		}

}
public void removeFromOrder(ActionEvent event) throws IOException{
	//TODO this.order.removeFromOrder(Dish);
}
	public void makeOrder(ActionEvent event) throws IOException{
		if(customer.getOrder().getDishes().getDishes().isEmpty()==true)
		{
		customer.setOrder(this.order,restaurant,customerIndex);
		confirmOrderButton.setText("Done!");
		System.out.println(this.order);
		}
		else
		{
			((Node)event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			primaryStage.initStyle(StageStyle.TRANSPARENT);
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/login.fxml").openStream());
			MainController mainController = (MainController)loader.getController();
			mainController.setRestaurant(restaurant);
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		}
		try {
			Data data = new Data();
			data.save(restaurant);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Total restaurant revenue = "+restaurant.getTotalMoney());
	}
	public void close(ActionEvent event) {
		System.exit(0);
	}
}
