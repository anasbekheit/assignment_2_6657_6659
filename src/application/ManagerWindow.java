package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mainRestaurant.Restaurant;
import parentClasses.Table;
import userTypes.User;

import java.util.concurrent.atomic.AtomicInteger;


public class ManagerWindow {
    public static void displayOptions(Restaurant restaurant)
    {
        Stage managerDshboard = new Stage();
        managerDshboard.initModality(Modality.APPLICATION_MODAL);
        managerDshboard.setTitle("Manager Dashboard");

        Label totalMoney = new Label("Total money earned by today:");
        Label totaltablesReserved = new Label("Tables reserved:");
        Label availableTables = new Label("Total Available Tables:");

        ObservableList<String> waitersNames= FXCollections.observableArrayList();
        ObservableList<String> cooksNames= FXCollections.observableArrayList();




        Label money = new Label();
        Label tablesReserved = new Label();
        Label tablesAvailable = new Label();



        Label cooks = new Label("Cooks in Restarant:");
        Label waiter = new Label("Waiters in Restaurant:");

        Button showCooks = new Button("Show");
        Button showWaiters = new Button( "Show");
        Button close = new Button("Close");
        Button showResults = new Button ("Show results");
        AtomicInteger counterTablesReserv= new AtomicInteger();
        AtomicInteger counterTableNotReserv= new AtomicInteger();
        showResults.setOnAction(e-> {
        	counterTablesReserv.set(0);
        	counterTableNotReserv.set(0);
            for(Table table : restaurant.getTables().getTables())
            {
            	
                if(table.isBooked()==true)
                {
                    counterTablesReserv.getAndIncrement();
                }
                else
                {
                    counterTableNotReserv.getAndIncrement();
                }
            }
            tablesReserved.setText(String.valueOf(counterTablesReserv.get()));
            tablesAvailable.setText(String.valueOf(counterTableNotReserv.get()));
            money.setText(String.valueOf(restaurant.getTotalMoney()));
        });


        showCooks.setOnAction( e-> {
            for(User user : restaurant.getUsers().getUserList())
            {
                if(user.getRole().equals("Cooker"))
                {

                    cooksNames.addAll(user.getName());
                }
            }
            showCooksList(cooksNames);
        });

        showWaiters.setOnAction(e-> {
            for(userTypes.User user : restaurant.getUsers().getUserList())
            {
                if(user.getRole().equals("Waiter"))
                {

                    waitersNames.addAll(user.getName());
                }
            }
            showWaitersList(waitersNames);
        });

        close.setOnAction( e -> {
            managerDshboard.close();
        });


        GridPane grid = new GridPane();
        grid.setPadding(new Insets(15,15,15,15));
        grid.setHgap(6);
        grid.setVgap(7);
        GridPane.setConstraints(totalMoney , 0,0);
        GridPane.setConstraints(money , 1,0);
        GridPane.setConstraints(totaltablesReserved , 0,1);
        GridPane.setConstraints(tablesReserved , 1,1 );
        GridPane.setConstraints(availableTables , 0,2);
        GridPane.setConstraints(tablesAvailable,1,2);
        GridPane.setConstraints(showResults , 0,3);
        GridPane.setConstraints(cooks,0,4);
        GridPane.setConstraints(showCooks, 1,4);
        GridPane.setConstraints(waiter , 0,5);
        GridPane.setConstraints(showWaiters , 1,5);
        GridPane.setConstraints(showResults , 2,3);
        grid.getChildren().addAll(totalMoney, money, totaltablesReserved, tablesReserved, availableTables, tablesAvailable, cooks, showCooks, waiter, showWaiters, showResults);
    
        Scene scene = new Scene(grid , 500    , 300);


        managerDshboard.setScene(scene);
        managerDshboard.show();

    }
    public static void showCooksList(ObservableList<String> cooks)
    {
        Stage cooksWindow = new Stage();
        cooksWindow.initModality(Modality.APPLICATION_MODAL);
        cooksWindow.setTitle("Cooks Window");
        ListView<String> cooksList = new ListView<>();
        cooksList.setItems(cooks);
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        Button close = new Button("Close");
        close.setAlignment(Pos.CENTER);
        close.setOnAction(e -> {
            cooksWindow.close();
        });
        vBox.getChildren().addAll(cooksList,close);
        Scene scene = new Scene(vBox , 400,400);
        cooksWindow.setScene(scene);
        cooksWindow.show();
    }
    public static void showWaitersList(ObservableList<String> waiters)
    {
        Stage waitersWindow = new Stage();
        waitersWindow.initModality(Modality.APPLICATION_MODAL);
        waitersWindow.setTitle("Cooks Window");
        ListView<String> waitersList = new ListView<>();
        waitersList.setItems(waiters);
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        Button close = new Button("Close");
        close.setAlignment(Pos.CENTER);
        close.setOnAction(e -> {
            waitersWindow.close();
        });
        vBox.getChildren().addAll(waitersList,close);
        Scene scene = new Scene(vBox , 400,400);
        waitersWindow.setScene(scene);
        waitersWindow.show();
    }
}