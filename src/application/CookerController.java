package application;
import userTypes.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import dataHandle.Data;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mainRestaurant.Restaurant;
import resandorder.Order;

public class CookerController implements Initializable {
	private Cooker cooker;
	
	private Restaurant restaurant;
	
	@FXML
	private Label textAreaLabel;
	@FXML
	private Label comboLabel;
	@FXML
	private TextArea textArea ;
	@FXML
	private ComboBox<Order> comboBox;
	@FXML
	private Button comboButton ;
	@FXML
	private Button logOutButton;
	ObservableList<Order> orderList= FXCollections.observableArrayList();
	//private int userIndex;
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		 comboBox.setItems(orderList);
	}

	public void setCooker(User user, int userIndex, Restaurant restaurant) {
		this.cooker= new Cooker(user.getName(),user.getRole(),user.getUsername(),user.getPassword());
	//	this.userIndex=userIndex;
		this.restaurant=new Restaurant();
		this.restaurant=restaurant;
			comboLabel.setText("Today's Orders");
			cooker= new Cooker(user.getName(),user.getRole(),user.getUsername(),user.getPassword());
			restaurant.getUsers().getCookers().getCookerList().add(cooker);
		//	userIndex=restaurant.getUsers().getCustomers().getCustomerList().indexOf(cooker);
		 for(Customer customer :restaurant.getUsers().getCustomers().getCustomerList()) {
			 if(customer.getOrder().getDishes().getDishes().isEmpty()==false) {
				 orderList.add(customer.getOrder());
				
			 }
			 textArea.setText(orderList.toString());
			 textArea.setEditable(false);
		 }
			
		}
	public void close(ActionEvent event) {
		System.exit(0);
	}			
	public void logOutButtonAction(ActionEvent event) throws IOException, JAXBException {
		Data data = new Data();
		data.save(restaurant);
		((Node)event.getSource()).getScene().getWindow().hide();
		Stage primaryStage = new Stage();
		primaryStage.initStyle(StageStyle.TRANSPARENT);
		FXMLLoader loader = new FXMLLoader();
		Pane root = loader.load(getClass().getResource("/login.fxml").openStream());
		MainController mainController = (MainController)loader.getController();
		mainController.setRestaurant(restaurant);
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	public void comboButtonAction(ActionEvent event) {
			if(comboBox.getValue()!=null) {
				
				cooker.cook(comboBox.getValue(), restaurant);
				orderList.remove(comboBox.getValue());
				textArea.clear();
				if(orderList.isEmpty()==false)
				{
					textArea.setText(orderList.toString());
				}
				else
				{
					textArea.setPromptText("No Orders For Now!");
				
				}
			
			}
			else {
				Alert.display("Error", "Please Select an Order To Cook");
			}
				}
			

}	
