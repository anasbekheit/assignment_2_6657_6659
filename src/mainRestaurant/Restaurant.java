package mainRestaurant;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "restaurant")
@XmlAccessorType (XmlAccessType.FIELD)
public class Restaurant {
	
	
	public Restaurant() {
		super();
		this.users = new Users();
		this.tables = new Tables();
		this.dishes = new Dishes();
	}
	@XmlElement(name = "users")
	private  Users users=new Users();
	@XmlElement(name = "tables")
	private  Tables tables= new Tables();
	@XmlElement(name="dishes")
	private   Dishes dishes = new Dishes();
	@XmlElement(name="totalMoney")
	private  double totalMoney;
	public  double getTotalMoney() {
		return totalMoney;
	}
	public  void setTotalMoney(double d) {
		this.totalMoney = d;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public Tables getTables() {
		return tables;
	}
	public void setTables(Tables tables) {
		this.tables = tables;
	}
	public Dishes getDishes() {
		return dishes;
	}
	public void setDishes(Dishes dishes) {
		this.dishes = dishes;
	}

}
