package mainRestaurant;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import userTypes.User;

import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "users")
@XmlAccessorType (XmlAccessType.FIELD)
public  class Users {
	public Users() {
		this.userList= new LinkedList<User>();
		// TODO Auto-generated constructor stub
	}

	@XmlElement(name = "Customers")
	private  Customers customers= new Customers();
	@XmlElement(name = "Cookers")
	private  Cookers cookers= new Cookers();
	@XmlElement(name = "Managers")
	private  Managers managers= new Managers();
	@XmlElement(name = "Waiters")
	private  Waiters waiters= new Waiters();
	@XmlElement(name= "user")
	private  LinkedList<User> userList= new LinkedList<User>();

	public  LinkedList<User> getUserList() {
		return userList;
	}

	public  void setUserList(LinkedList<User> userList) {
		this.userList = userList;
	}

	public Customers getCustomers() {
		return customers;
	}

	public void setCustomers(Customers customers) {
		this.customers = customers;
	}

	public Cookers getCookers() {
		return cookers;
	}

	public void setCookers(Cookers cookers) {
		this.cookers = cookers;
	}

	public Managers getManagers() {
		return managers;
	}

	public void setManagers(Managers managers) {
		this.managers = managers;
	}

	public Waiters getWaiters() {
		return waiters;
	}

	public void setWaiters(Waiters waiters) {
		this.waiters = waiters;
	}





}
