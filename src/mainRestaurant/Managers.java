package mainRestaurant;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import userTypes.Manager;


@XmlRootElement(name = "Managers")
@XmlAccessorType (XmlAccessType.FIELD)
public  class Managers {
	
	@XmlElement(name= "manager")
	private  LinkedList<Manager> managerList= new LinkedList<Manager>();
	public Managers() {
		this.managerList= new LinkedList<Manager>();
		// TODO Auto-generated constructor stub
	}
	public LinkedList<Manager> getManagerList() {
		return managerList;
	}
	public void setManagerList(LinkedList<Manager> managerList) {
		this.managerList = managerList;
	}

}
