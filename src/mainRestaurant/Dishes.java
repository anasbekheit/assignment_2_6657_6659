package mainRestaurant;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import parentClasses.Dish;

@XmlRootElement(name = "dishes")
@XmlAccessorType (XmlAccessType.FIELD)
public class Dishes {
	public Dishes() {
		this.dishes= new LinkedList<Dish>();
		// TODO Auto-generated constructor stub
	}

	@XmlElement(name= "dish")
	private  LinkedList<Dish> dishes= new LinkedList<Dish>();

	public  LinkedList<Dish> getDishes() {
		return dishes;
	}

	public  void setDishes(LinkedList<Dish> dishes) {
		this.dishes = dishes;
	}
}