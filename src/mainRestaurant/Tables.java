package mainRestaurant;
import java.util.LinkedList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import parentClasses.Table;

@XmlRootElement(name = "tables")
@XmlAccessorType (XmlAccessType.FIELD)
public class Tables {
	public Tables() {
		this.tables= new LinkedList<Table>();
		// TODO Auto-generated constructor stub
	}

	@XmlElement(name= "table")
	private  LinkedList<Table> tables= new LinkedList<Table>();

	public  LinkedList<Table> getTables() {
		return tables;
	}

	public  void setTables(LinkedList<Table> tables) {
		this.tables = tables;
	}
	
	public String toString() {
		String result="";
		for(Table table:this.getTables()) {
		result+=table+"\n";	
		}
		
		
		return result;
	}

}
