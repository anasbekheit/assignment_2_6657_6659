package mainRestaurant;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import userTypes.Waiter;

import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "Waiters")
@XmlAccessorType (XmlAccessType.FIELD)
public  class Waiters {
	
	@XmlElement(name= "waiter")
	private  LinkedList<Waiter> waiterList= new LinkedList<Waiter>();
	public Waiters() {
		this.waiterList= new LinkedList<Waiter>();
		// TODO Auto-generated constructor stub
	}
	public LinkedList<Waiter> getWaiterList() {
		return waiterList;
	}
	public void setWaiterList(LinkedList<Waiter> waiterList) {
		this.waiterList = waiterList;
	}	
}