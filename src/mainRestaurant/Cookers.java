package mainRestaurant;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import userTypes.Cooker;

import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "Cookers")
@XmlAccessorType (XmlAccessType.FIELD)
public  class Cookers {
	
	@XmlElement(name= "cook")
	private  LinkedList<Cooker> cookerList= new LinkedList<Cooker>();
	public Cookers() {
		this.cookerList= new LinkedList<Cooker>();
		// TODO Auto-generated constructor stub
	}	

	public  LinkedList<Cooker> getCookerList() {
		return cookerList;
	}

	public  void setCooker(LinkedList<Cooker> cookerList) {
		this.cookerList = cookerList;
	}

	public void setCookerList(LinkedList<Cooker> cookerList) {
		this.cookerList = cookerList;
	}
}