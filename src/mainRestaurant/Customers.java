package mainRestaurant;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import userTypes.Customer;

import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "Customers")
@XmlAccessorType (XmlAccessType.FIELD)
public  class Customers {
	
	@XmlElement(name= "customer")
	private  LinkedList<Customer> customerList= new LinkedList<Customer>();
	public Customers() {
		this.customerList= new LinkedList<Customer>();
		// TODO Auto-generated constructor stub
	}

	

	public  LinkedList<Customer> getCustomerList() {
		return customerList;
	}

	public  void setUserList(LinkedList<Customer> customerList) {
		this.customerList = customerList;
	}


public String toString() {
	String result="";
	
	for(Customer customer:this.getCustomerList()) {
		result+=customer;
	}
	result+="\n";
	
	
	return result;
	
}



public void setCustomerList(LinkedList<Customer> customerList) {
	this.customerList = customerList;
}
}
