package resandorder;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import mainRestaurant.Dishes;
import parentClasses.Dish;
@XmlRootElement(name = "order")
@XmlAccessorType (XmlAccessType.FIELD)
 public class Order {
	
	@XmlElement(name="totalPrice")
	private double totalPrice=0;
	
	private boolean cooked=false;
	@XmlElement(name="tableNumber")
	private Integer tableNumber =-1;
	
	@XmlElement(name="dishes")
	private Dishes dishes= new Dishes();

	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	} 
	public void addToOrder(Dish Dish){
		int checkIndex=dishInOrder(Dish);
		if(checkIndex!=-1)
		{	this.getDishes().getDishes().get(checkIndex).setCount(this.getDishes().getDishes().get(checkIndex).getCount()+1);
			
		System.out.println("WE GOT HERE"+this.getDishes().getDishes().get(checkIndex).getCount());
		}
		else {
					Dish.setCount(1);
					this.dishes.getDishes().add(Dish);
		}
		this.totalPrice+=Dish.getTotalPrice();
	}
	private int dishInOrder(Dish dish) {
		for(Dish dishh:this.dishes.getDishes()) {
			if(dishh.getName().equals(dish.getName())) {
				return this.dishes.getDishes().indexOf(dishh);
			}
		}
		return -1;
	}
	public void removeFromOrder(Dish Dish) {
		this.dishes.getDishes().remove(Dish);
		this.totalPrice-=Dish.getTotalPrice();
	}
	public Integer getTableNumber() {
		return tableNumber;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Dishes getDishes() {
		return dishes;
	}
	public void setDishes(Dishes dishes) {
		this.dishes = dishes;
	}
	public void setTableNumber(Integer tableNumber) {
		this.tableNumber = tableNumber;
	}
	public String toString() {
        String result = "Table:"+this.getTableNumber()+")\n";
       for(Dish dish:this.getDishes().getDishes()) {
    	   result+= dish.getCount()+"x"+dish.getName()+"\n";
       }
       result+="\n----------------------------\n";
        return  result;
}
	public boolean isCooked() {
		return cooked;
	}
	public void setCooked(boolean cooked) {
		this.cooked = cooked;
	}
}
