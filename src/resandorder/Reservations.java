package resandorder;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "pastReservations")
@XmlAccessorType (XmlAccessType.FIELD)
public class Reservations {
	@XmlElement(name="orders")
	private Orders orders= new Orders();
	
	public Orders getReservations() {
		return orders;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

}
