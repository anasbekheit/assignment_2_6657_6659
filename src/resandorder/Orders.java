package resandorder;

import java.util.LinkedList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "orders")
@XmlAccessorType (XmlAccessType.FIELD)
public class Orders {
	
	@XmlElement(name= "order")
	LinkedList<Order> Orders = new LinkedList<Order>();

	public LinkedList<Order> getOrders() {
		return Orders;
	}

	public void setOrders(LinkedList<Order> orders) {
		Orders = orders;
	}
	
	
public String toString() {
	if(this.getOrders().isEmpty()==false)
	{
			String result="";
	for(Order order:this.getOrders()) {
		result+="Table Number :"+order.getTableNumber()+"\n";
	}
	return result;
	}
	else {
		return "NO ORDERS FOR TODAY";
	}
	
}
}

