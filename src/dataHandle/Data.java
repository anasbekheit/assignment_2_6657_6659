package dataHandle;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import mainRestaurant.Restaurant;

public class Data  {
	
	public  Restaurant load() throws JAXBException{
		 Restaurant restaurant = new Restaurant();
		JAXBContext jaxbContext = null;
		try {
			jaxbContext = JAXBContext.newInstance(Restaurant.class);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Unmarshaller unmarshaller = null;
		try {
			unmarshaller = jaxbContext.createUnmarshaller();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		try {
			restaurant = (Restaurant) unmarshaller.unmarshal(new File ("C:\\Users\\Ayman\\Desktop\\Java\\OOP Project_2\\src\\datainput.xml"));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(restaurant.getTotalMoney());
		return restaurant;
		
	}
	
	public void save(Restaurant restaurant) throws JAXBException{
		JAXBContext jaxbContext = null;
		try {
			jaxbContext = JAXBContext.newInstance(Restaurant.class);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.marshal(restaurant,new File ("C:\\Users\\Ayman\\Desktop\\Java\\OOP Project_2\\src\\saved.xml"));
	}
}
