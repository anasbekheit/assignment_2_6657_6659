package userTypes;

import java.util.LinkedList;

import mainRestaurant.Restaurant;
import parentClasses.Table;

public class Manager extends User {
	public Manager() {
		super();
	}	
	private LinkedList<Table> reservedTables = new LinkedList<Table> ();
	public Manager(String name, String role, String username, String password) {
		super(name, role, username, password);
	}
	
public double getEarnings(Restaurant restaurant)
{
	return restaurant.getTotalMoney();
}
public LinkedList<Table> getReservedTables(Restaurant restaurant)
{
	for (Table table:restaurant.getTables().getTables())
	{
		if(table.isBooked()==true)
		{
			reservedTables.add(table);
		}
	}
		return reservedTables;
	}

}
