package userTypes;
import mainRestaurant.Restaurant;
import resandorder.Order;

public class Cooker extends User {
	
	public Cooker() {
		super();
	}	

	
	
	public Cooker(String name, String role, String username, String password) {
		super(name, role, username, password);
		// TODO Auto-generated constructor stub
	}
	public void cook(Order order,Restaurant restaurant) {
		for(Customer customer1 : restaurant.getUsers().getCustomers().getCustomerList()) {
			if(customer1.getOrder().equals(order)) {
				order.setCooked(true);
				customer1.setOrder(order, restaurant, restaurant.getUsers().getCustomers().getCustomerList().indexOf(customer1));
			}
		}
		

}
}
