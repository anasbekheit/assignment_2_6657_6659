package userTypes;
import javax.xml.bind.JAXBException;

import dataHandle.Data;
import mainRestaurant.Restaurant;
import parentClasses.Table;
public class Waiter extends User {
	public Waiter() {
		super();
	}	
	
	public Waiter(String name, String role, String username, String password) {
		super(name, role, username, password);
		// TODO Auto-generated constructor stub
	}

	public void serveOrder(Table table,Restaurant restaurant) {
		for(Customer customer1 : restaurant.getUsers().getCustomers().getCustomerList()) {
			if(customer1.getName().equals(table.getCustomerName())) {
				Table emptyTable = new Table();
				emptyTable.setSmoking(table.isSmoking());
				emptyTable.setNumber(table.getNumber());
				emptyTable.setNumber_of_seats(table.getNumber_of_seats());
				Data data = new Data();
				try {
					data.save(restaurant);
				} catch (JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				customer1.getPastReservations().getOrders().getOrders().add(table.getOrder());
				customer1.setOrder(null, restaurant, restaurant.getUsers().getCustomers().getCustomerList().indexOf(customer1));
			}
		}
	}
	}

