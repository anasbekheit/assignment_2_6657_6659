package userTypes;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mainRestaurant.Restaurant;
import parentClasses.Table;
import resandorder.Order;
import resandorder.Reservations;
@XmlRootElement(name="customer")
public class Customer extends User {
	
	@XmlElement(name="totalRevenue")
	private double totalRevenue=0;
	private  Order currentOrder=new Order(); 
	private Reservations pastReservations = new Reservations();
	private int tableIndex;
	public Customer() {
		super();
	}	
	
	public Customer(String name, String role, String username, String password) {
		super(name, role, username, password);

	}



	public Table reserveTable(int seats,boolean smoke,Restaurant restaurant) {
	    for(Table x:restaurant.getTables().getTables())
	    {
	    	if(x.isSmoking()==smoke && x.getNumber_of_seats()==seats && x.isBooked()==false)
	    	{	
	    		x.setCustomerName(this.getName());
	    		x.setBooked(true);
	    		this.setTableIndex(restaurant.getTables().getTables().indexOf(x));
	    		
	    		return x;
	    	}
	    }
	    return null;
	}
	

	



	public int getTableIndex() {
		return tableIndex;
	}

	public void setTableIndex(int tableIndex) {
		this.tableIndex = tableIndex;
	}

	public Order getOrder() {
		return currentOrder;
	}

	public void setOrder(Order order,Restaurant restaurant,int customerIndex) {
		if(order!=null)
		{
			order.setTableNumber(restaurant.getTables().getTables().get(this.getTableIndex()).getNumber());
			this.totalRevenue+=order.getTotalPrice();
			restaurant.setTotalMoney(restaurant.getTotalMoney()+order.getTotalPrice());
		}	
		restaurant.getTables().getTables().get(this.getTableIndex()).setOrder(order);
		restaurant.getUsers().getCustomers().getCustomerList().set(customerIndex,this);
		this.currentOrder = order;
		
	}

	public Reservations getPastReservations() {
		return pastReservations;
	}

	public void setPastReservations(Reservations pastReservations) {
		this.pastReservations = pastReservations;
	}

	public double getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}
	public String toString() {
		
		return String.format("Customer Name: %s\nAmount Paid: %.2f\nOrdered Dishes: %s", this.getName(),this.getOrder().getTotalPrice(),this.getOrder());
	}
	
}

